/// <reference types="cypress" />
describe('Interações com select, radio e checks',() =>{

    it('Validando interações',()=>{
        cy.visit('http://webdriveruniversity.com/Dropdown-Checkboxes-RadioButtons/index.html')
        cy.get("#dropdowm-menu-1").select('c#')
        cy.get('[type="checkbox"]').check("option-1")
        .should("be.checked")
        cy.get('[type="checkbox"]').uncheck("option-3")
        .should("not.be.checked")
        cy.get("input[value=blue]").click()
        cy.get("input[value=yellow]").check()
        cy.get("input[value=blue]").should("not.be.checked")

    })

})